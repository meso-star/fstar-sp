/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the StarSP library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef SSP_COMPLEMENTS_H
#define SSP_COMPLEMENTS_H

#include <rsys/rsys.h>
#include <star/ssp.h>

/* Library symbol management */
#if defined(FSP_SHARED_BUILD) /* Build shared library */
  #define FSP_API extern EXPORT_SYM
#elif defined(FSP_STATIC) /* Use/build static library */
  #define FSP_API extern LOCAL_SYM
#else /* Use shared library */
  #define FSP_API extern IMPORT_SYM
#endif

BEGIN_DECLS

FSP_API _Bool SSP_RNG_TYPE_EQ(const struct ssp_rng_type* t0, const struct ssp_rng_type* t1);

/*******************************************************************************
 * Sphere distribution
 ******************************************************************************/
/* Uniform sampling of an unit sphere. The PDF of the generated sample is
 * stored in sample[3] */
FSP_API float* SSP_RAN_SPHERE_UNIFORM(struct ssp_rng* rng, float sample[4]);

/* Return the probability distribution for a sphere uniform random variate */
FSP_API float SSP_RAN_SPHERE_UNIFORM_PDF(void);

/*******************************************************************************
 * Triangle distribution
 ******************************************************************************/
/* Uniform sampling of a triangle */
FSP_API float* SSP_RAN_TRIANGLE_UNIFORM
  (struct ssp_rng* rng,
   const float v0[3], const float v1[3],  const float v2[3],/*triangle vertices*/
   float sample[3]); /* Sampled position */

FSP_API float SSP_RAN_TRIANGLE_UNIFORM_PDF
  (const float v0[3],
   const float v1[3],
   const float v2[3]);

/*******************************************************************************
 * Hemisphere distribution
 ******************************************************************************/
/* Uniform sampling of an unit hemisphere whose up direction is implicitly the
 * Z axis. The PDF of the generated sample is stored in sample[3] */
FSP_API float* SSP_RAN_HEMISPHERE_UNIFORM_LOCAL(struct ssp_rng* rng, float sample[4]);

/* Return the probability distribution for an hemispheric uniform random
 * variate with an implicit up direction in Z */
FSP_API float SSP_RAN_HEMISPHERE_UNIFORM_LOCAL_PDF(const float sample[3]);

/* Uniform sampling of an unit hemisphere whose up direction is `up'. The PDF
 * of the generated sample is stored in sample[3] */
FSP_API float* SSP_RAN_HEMISPHERE_UNIFORM
  (struct ssp_rng* rng, const float up[3], float sample[4]);

/* Return the probability distribution for an hemispheric uniform random
 * variate with an explicit `up' direction */
FSP_API float SSP_RAN_HEMISPHERE_UNIFORM_PDF(const float up[3], const float sample[3]);

/* Cosine weighted sampling of an unit hemisphere whose up direction is
 * implicitly the Z axis. The PDF of the generated sample is stored in
 * sample[3] */
FSP_API float* SSP_RAN_HEMISPHERE_COS_LOCAL(struct ssp_rng* rng, float sample[4]);

/* Return the probability distribution for an hemispheric cosine weighted
 * random variate with an implicit up direction in Z */
FSP_API float SSP_RAN_HEMISPHERE_COS_LOCAL_PDF(const float sample[3]);

/* Cosine weighted sampling of an unit hemisphere whose up direction is `up'.
 * The PDF of the generated sample is stored in sample[3] */
FSP_API float* SSP_RAN_HEMISPHERE_COS(struct ssp_rng* rng, const float up[3], float sample[4]);

/* Return the probability distribution for an hemispheric cosine weighted
 * random variate with an explicit `up' direction */
FSP_API float SSP_RAN_HEMISPHERE_COS_PDF(const float up[3], const float sample[3]);

/*******************************************************************************
 * Henyey Greenstein distribution
 ******************************************************************************/

/* Return the probability distribution for a Henyey-Greenstein random
 * variate with an implicit incident direction in Z */
FSP_API float SSP_RAN_SPHERE_HG_LOCAL_PDF(const double g, const float sample[3]);

/* Henyey-Greenstein sampling of an unit sphere for an incident direction 
 * that is implicitly the Z axis. 
 * The PDF of the generated sample is stored in sample[3] */
FSP_API float* SSP_RAN_SPHERE_HG_LOCAL(struct ssp_rng* rng, const double g, float sample[4]);

/* Return the probability distribution for a Henyey-Greenstein random
 * variate with an explicit incident direction that is `up' */
FSP_API float SSP_RAN_SPHERE_HG_PDF(const float up[3], const double g, const float sample[3]);

/* Henyey-Greenstein sampling of an unit sphere for an incident direction
 * that is `up'. 
 * The PDF of the generated sample is stored in sample[3] */
FSP_API float* SSP_RAN_SPHERE_HG(struct ssp_rng* rng, const float up[3], const double g, float sample[4]);

END_DECLS

#endif
