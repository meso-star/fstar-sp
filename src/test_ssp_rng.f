! Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
!
! This software is a program whose purpose is to test the spp library.
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

#include "ssp.inc"

#define SZT(n) INT(n, C_SIZE_T)

! (1): Special warning about some API calls manipulating int64_t C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT64_T
! where C_UINT64_T (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint64 will be treated as negative)
! A workaround is to TRANFER the bytes of these unsigned-stored-in-a-signed
! to int128 (see below)

SUBROUTINE TEST_RNG(type)
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_FLOAT, C_DOUBLE, C_PTR, C_LOC, C_ASSOCIATED, C_NULL_PTR
  USE RSYS_BINDING
  USE RSYS_MEM_ALLOCATOR_BINDING
  USE RSYS_STDIO_BINDING
  USE SSP_BINDING
  IMPLICIT NONE

  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  LOGICAL :: CAN_SET, CAN_RW, CAN_HAVE_ENTROPY, CAN_DISCARD, IS_PSEUDO
  INTEGER(C_INT) :: RES, I, J
  INTEGER(C_INT64_T) :: R, I64_1, I64_2
  INTEGER, PARAMETER :: NRAND = 1024
  INTEGER(C_INT64_T), DIMENSION(NRAND) :: datai64_1, datai64_2
  INTEGER(KIND=16), DIMENSION(NRAND) :: datai128_1, datai128_2 ! See (1)
  INTEGER(KIND=16) :: MIN, MAX, I128_1, I128_2 ! See (1)
  REAL(C_DOUBLE) :: D_1, D_2
  REAL(C_FLOAT) :: F_1, F_2
  REAL(C_DOUBLE), DIMENSION(NRAND) :: datad
  REAL(C_FLOAT), DIMENSION(NRAND) :: dataf
  TYPE(C_PTR) :: RNG1, RNG2
  TYPE(MEM_ALLOCATOR), TARGET :: ALLOCATOR
  TYPE(C_PTR) :: STREAM
  INTEGER(C_INT) :: foo ! to discard unused function returns

  CAN_SET = type .NE. SSP_RNG_RANDOM_DEVICE
  CAN_RW = type .NE. SSP_RNG_RANDOM_DEVICE
  CAN_HAVE_ENTROPY = type .EQ. SSP_RNG_RANDOM_DEVICE
  CAN_DISCARD = type .NE. SSP_RNG_RANDOM_DEVICE
  IS_PSEUDO = type .NE. SSP_RNG_RANDOM_DEVICE

  RES = MEM_INIT_PROXY_ALLOCATOR(ALLOCATOR, MEM_DEFAULT_ALLOCATOR)

  CHECK(SSP_RNG_CREATE(C_LOC(ALLOCATOR), type, RNG1), RES_OK)
  CHECK(SSP_RNG_CREATE(C_LOC(ALLOCATOR), type, RNG2), RES_OK)

  RES = SSP_RNG_DISCARD(RNG1, SZT(10))
  IF (CAN_DISCARD) THEN
    CHECK(RES, RES_OK)
  ELSE
    CHECK(RES, RES_BAD_OP)
  ENDIF
  IF (IS_PSEUDO .AND. CAN_DISCARD) THEN
    DO I = 1, 10
       R = SSP_RNG_GET(RNG2)
    ENDDO
    CHECK(SSP_RNG_GET(RNG1), SSP_RNG_GET(RNG2))
  ENDIF

  ! because of the lack of unsigned in Fortran
  ! TRANSFER must be used to copy the binary repesentation
  ! of the uint64_t returned by SSP API calls
  ! into an int128 (see (1))
  ! (C_SIZE_T integers cannot do the trick)
  MIN = TRANSFER(SSP_RNG_MIN(RNG1), MIN)
  MAX = TRANSFER(SSP_RNG_MAX(RNG1), MIN)
  DO I = 1, NRAND
    datai128_1(I) = TRANSFER(SSP_RNG_GET(RNG1), MIN)
    CHECK_TRUE(datai128_1(I) .GE. MIN)
    CHECK_TRUE(datai128_1(I) .LE. MAX)
    DO J = 1, I - 1
      NCHECK(datai128_1(I), datai128_1(J))
    ENDDO
  ENDDO

  ! See (1) for TRANSFER explanation
  RES = SSP_RNG_SET(RNG1, TRANSFER(Z'DECAFBAD', R))
  IF (CAN_SET) THEN
    CHECK(RES, RES_OK)
  ELSE
    CHECK(RES, RES_BAD_OP)
  ENDIF

  DO I = 1, NRAND
    ! See (1) for TRANSFER explanation
    datai128_2(I) =  TRANSFER(SSP_RNG_GET(RNG1), MIN)
    NCHECK(datai128_2(I), datai128_1(I))
    CHECK_TRUE(datai128_2(I) .GE. MIN)
    CHECK_TRUE(datai128_2(I) .LE. MAX)
    DO J = 1, I - 1
      NCHECK(datai128_2(I), datai128_2(J))
    ENDDO
  ENDDO

  ! Here bounds fit in signed int64_t
  ! No need to bother with int128_t; See (1)
  I64_1 = 1
  I64_2 = 79
  DO I = 1, NRAND
    datai64_1(I) = SSP_RNG_UNIFORM_UINT64(RNG1, I64_1, I64_2)
    CHECK_TRUE(datai64_1(I) .GE. I64_1)
    CHECK_TRUE(datai64_1(I) .LE. I64_2)
  ENDDO

  DO I = 1, NRAND
    DO J = 1, NRAND
      IF (datai64_1(I) .NE. datai64_1(J)) EXIT
    ENDDO
    NCHECK(J, NRAND)
  ENDDO

  ! The 2 following TRANSFER are just needed to use hex constants
  I128_1 = TRANSFER(Z'8000000000000000', I128_1)
  I128_2 = TRANSFER(Z'A000000000000000', I128_1)
  ! Here bounds fit in uint64_t but don't in int64_t
  ! There no option but bothering with int128_t
  DO I = 1, NRAND
    ! See (1) for TRANSFER explanation
    datai128_1(I) = TRANSFER(SSP_RNG_UNIFORM_UINT64(RNG1, TRANSFER(I128_1, R), TRANSFER(I128_2, R)), MIN)
    CHECK_TRUE(datai128_1(I) .GE. I128_1)
    CHECK_TRUE(datai128_1(I) .LE. I128_2)
  ENDDO

  DO I = 1, NRAND
    DO J = 1, NRAND
      IF (datai128_1(I) .NE. datai128_1(J)) EXIT
    ENDDO
    NCHECK(J, NRAND)
  ENDDO

  D_1 = -5.0
  D_2 = 11.3
  DO I = 1, NRAND
    datad(I) = SSP_RNG_UNIFORM_DOUBLE(RNG1, D_1, D_2)
    CHECK_TRUE(datad(I) .GE. D_1)
    CHECK_TRUE(datad(I) .LE. D_2)
  ENDDO

  DO I = 1, NRAND
    DO J = 1, NRAND
      IF (datad(I) .NE. datad(J)) EXIT
    ENDDO
    NCHECK(J, NRAND)
  ENDDO

  F_1 = -1.0
  F_2 = 12.5
  DO I = 1, NRAND
    dataf(I) = SSP_RNG_UNIFORM_FLOAT(RNG1, F_1, F_2)
    CHECK_TRUE(dataf(I) .GE. F_1)
    CHECK_TRUE(dataf(I) .LE. F_2)
  ENDDO

  DO I = 1, NRAND
    DO J = 1, NRAND
      IF (dataf(I) .NE. dataf(J)) EXIT
    ENDDO
    NCHECK(J, NRAND)
  ENDDO

  DO I = 1, NRAND
    datad(I) = SSP_RNG_CANONICAL(RNG1)
    CHECK_TRUE(datad(I) .GE. 0.0)
    CHECK_TRUE(datad(I) .LT. 1.0)
    DO J = 1, I - 1
      NCHECK(datad(I), datad(J))
    ENDDO
  ENDDO

  DO I = 1, NRAND
    dataf(I) = SSP_RNG_CANONICAL_FLOAT(RNG1)
    CHECK_TRUE(dataf(I) .GE. 0.0)
    CHECK_TRUE(dataf(I) .LT. 1.0)
    DO J = 1, I - 1
      NCHECK(dataf(I), dataf(J))
    ENDDO
  ENDDO

  IF (CAN_HAVE_ENTROPY) THEN
    WRITE(*,*) "Entropy for this implementation and system: ", ssp_rng_entropy(RNG1)
  END IF

  STREAM = C_TMPFILE()
  CHECK_TRUE(C_ASSOCIATED(STREAM))
  CHECK(SSP_RNG_WRITE(C_NULL_PTR, C_NULL_PTR), RES_BAD_ARG)
  CHECK(SSP_RNG_WRITE(RNG1, C_NULL_PTR), RES_BAD_ARG)
  CHECK(SSP_RNG_WRITE(C_NULL_PTR, STREAM), RES_BAD_ARG)
  IF (CAN_RW) THEN
    CHECK(SSP_RNG_WRITE(RNG1, STREAM), RES_OK)
  ELSE
    CHECK(SSP_RNG_WRITE(RNG1, STREAM), RES_BAD_OP)
  END IF

  DO I = 1, NRAND
    datai64_1(I) = SSP_RNG_GET(RNG1)
  END DO

  CHECK(SSP_RNG_READ(C_NULL_PTR, C_NULL_PTR), RES_BAD_ARG)
  CHECK(SSP_RNG_READ(RNG1, C_NULL_PTR), RES_BAD_ARG)
  CHECK(SSP_RNG_READ(C_NULL_PTR, STREAM), RES_BAD_ARG)
  IF (CAN_RW) THEN
    CHECK(SSP_RNG_READ(RNG1, STREAM), RES_IO_ERR)
  ELSE
    CHECK(SSP_RNG_READ(RNG1, STREAM), RES_BAD_OP)
  END IF
  CALL C_REWIND(STREAM)
  IF (CAN_RW) THEN
    CHECK(SSP_RNG_READ(RNG1, STREAM), RES_OK)
  ELSE
    CHECK(SSP_RNG_READ(RNG1, STREAM), RES_BAD_OP)
  END IF
  

  IF (CAN_RW) THEN
    DO I = 1, NRAND
      CHECK(SSP_RNG_GET(RNG1), datai64_1(I))
    END DO
  END IF
  foo = C_FCLOSE(STREAM)

  CHECK(SSP_RNG_REF_PUT(RNG1), RES_OK)
  CHECK(SSP_RNG_REF_PUT(RNG2), RES_OK)
  CALL CHECK_MEMORY_ALLOCATOR(ALLOCATOR)
  CALL MEM_SHUTDOWN_PROXY_ALLOCATOR(ALLOCATOR)

END SUBROUTINE TEST_RNG


PROGRAM test
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR, C_LOC
  USE RSYS_BINDING
  USE SSP_BINDING
  IMPLICIT NONE

  CALL TEST_RNG(SSP_RNG_KISS)
  CALL TEST_RNG(SSP_RNG_MT19937_64)
  CALL TEST_RNG(SSP_RNG_RANLUX48)
  CALL TEST_RNG(SSP_RNG_RANDOM_DEVICE)
  CALL TEST_RNG(SSP_RNG_THREEFRY)

END 
