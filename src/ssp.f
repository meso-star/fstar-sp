! Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the StarSP library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

MODULE SSP_BINDING

! (1): Special warning about some API calls manipulating uint64_t C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT64_T
! where C_UINT64_T (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint64_t will be treated as negative)
! A workaround is to TRANSFER the bytes of these unsigned-stored-in-a-signed
! to int128 (see below)

USE ISO_C_BINDING, ONLY: C_FUNPTR, C_PTR, C_SIZE_T, C_INT64_T, C_DOUBLE
IMPLICIT NONE

! Generic Random Number Generator type descriptor
 TYPE, BIND(C) :: SSP_RNG_TYPE
  TYPE(C_FUNPTR) :: INIT
  TYPE(C_FUNPTR) :: RELEASE
  TYPE(C_FUNPTR) :: SET
  TYPE(C_FUNPTR) :: GET
  TYPE(C_FUNPTR) :: DISCARD
  TYPE(C_FUNPTR) :: READ
  TYPE(C_FUNPTR) :: WRITE
  TYPE(C_FUNPTR) :: ENTROPY

  INTEGER(C_INT64_T) :: MIN ! See (1)
  INTEGER(C_INT64_T) :: MAX ! See (1)
  INTEGER(C_SIZE_T) :: SIZEOF_STATE
 END TYPE SSP_RNG_TYPE

 INTERFACE OPERATOR(.EQ.)
  MODULE PROCEDURE ssp_rng_type_eq
 END INTERFACE

 INTERFACE OPERATOR(.NE.)
  MODULE PROCEDURE ssp_rng_type_neq
 END INTERFACE

! The different generators available

! David Jones's Keep It Simple Stupid builtin PRNG type.
! Suitable for fast basic randomness
TYPE(SSP_RNG_TYPE), BIND(C, NAME='ssp_rng_kiss'), TARGET :: SSP_RNG_KISS

! 64-bits Mersenne Twister PRNG type of Matsumoto and Nishimura, 2000
TYPE(SSP_RNG_TYPE), BIND(C, NAME='ssp_rng_mt19937_64'), TARGET :: SSP_RNG_MT19937_64

! 48-bits RANLUX builtin PRNG type of Lusher and James, 1994
TYPE(SSP_RNG_TYPE), BIND(C, NAME='ssp_rng_ranlux48'), TARGET :: SSP_RNG_RANLUX48

! A random_device RNG is a uniformly-distributed integer RNG
! that produces non-deterministic random numbers. 
! It may be implemented in terms of an implementation-defined 
! pseudo-random number engine if a non-deterministic source 
! (e.g. a hardware device) is not available to the implementation.
! In this case each random_device object may generate the same
! number sequence.
TYPE(SSP_RNG_TYPE), BIND(C, NAME='ssp_rng_random_device'), TARGET :: SSP_RNG_RANDOM_DEVICE

! One of the counter-based PRNG of Salmon, Moraes, Dror & Shaw
TYPE(SSP_RNG_TYPE), BIND(C, NAME='ssp_rng_threefry'), TARGET :: SSP_RNG_THREEFRY

! Random Number Generator API

INTERFACE

! create a generator from its type using a given allocator 
! returns a code from res_T enum (see rsys.inc)
 FUNCTION ssp_rng_create(allocator, type, rng) BIND(C, NAME='ssp_rng_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SSP_RNG_TYPE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator ! May be C_NULL_PTR <=> use default allocator
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  TYPE(C_PTR), INTENT(OUT) :: rng
  INTEGER(C_INT) :: ssp_rng_create
 END FUNCTION ssp_rng_create

! release a reference to a rng
! returns a code from res_T enum (see rsys.inc)
 FUNCTION ssp_rng_ref_put(rng) BIND(C, NAME='ssp_rng_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT) :: ssp_rng_ref_put
 END FUNCTION ssp_rng_ref_put

! take a reference to a rng
! returns a code from res_T enum (see rsys.inc)
 FUNCTION ssp_rng_ref_get(rng) BIND(C, NAME='ssp_rng_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT) :: ssp_rng_ref_get
 END FUNCTION ssp_rng_ref_get

! get the type of a rng
! returns a code from res_T enum (see rsys.inc)
 FUNCTION ssp_rng_get_type(rng, type) BIND(C, NAME='ssp_rng_get_type')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SSP_RNG_TYPE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  INTEGER(C_INT) :: ssp_rng_get_type
 END FUNCTION ssp_rng_get_type

! (re)set the seed of a rng
! returns a code from res_T enum (see rsys.inc)
!
! The seed parameter is uint64_t C-side
! But ISO_C_BINDING limitations force to declare it as int64_t
! To avoid overflows, this value should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION ssp_rng_set(rng, seed) BIND(C, NAME='ssp_rng_set')
  USE ISO_C_BINDING, ONLY: C_INT, C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T), INTENT(IN), VALUE :: seed ! See (1)
  INTEGER(C_INT) :: ssp_rng_set
 END FUNCTION ssp_rng_set

! Uniform random distribution in [ssp_rng_min(rng), ssp_rng_max(rng)]
!
! The returned value is uint64_t C-side
! But ISO_C_BINDING limitations force to declare them as int64_t
! To avoid overflows, these values should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION ssp_rng_get(rng) BIND(C, NAME='ssp_rng_get')
  USE ISO_C_BINDING, ONLY: C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T) :: ssp_rng_get ! See (1)
 END FUNCTION ssp_rng_get

! discard the n next random numbers
! returns a code from res_T enum (see rsys.inc)
 FUNCTION ssp_rng_discard(rng, n) BIND(C, NAME='ssp_rng_discard')
  USE ISO_C_BINDING, ONLY: C_INT, C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T), INTENT(IN), VALUE :: n
  INTEGER(C_INT) :: ssp_rng_discard
 END FUNCTION ssp_rng_discard

! compare to rng types
 FUNCTION ssp_rng_type_eq(rng_type_1, rng_type_2) BIND(C, NAME='SSP_RNG_TYPE_EQ')
  USE ISO_C_BINDING, ONLY: C_BOOL, C_PTR
  IMPORT SSP_RNG_TYPE
  LOGICAL(C_BOOL) ssp_rng_type_eq
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng_type_1
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng_type_2
 END FUNCTION ssp_rng_type_eq

! Uniform random distribution in [lower upper]
!
! The returned value as well as lower and upper are uint64_t C-side
! But ISO_C_BINDING limitations force to declare them as int64_t
! To avoid overflows, these values should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION ssp_rng_uniform_uint64(rng, lower, upper) BIND(C, NAME='ssp_rng_uniform_uint64')
  USE ISO_C_BINDING, ONLY: C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T), INTENT(IN), VALUE :: lower ! See (1)
  INTEGER(C_INT64_T), INTENT(IN), VALUE :: upper ! See (1)
  INTEGER(C_INT64_T) :: ssp_rng_uniform_uint64 ! See (1)
 END FUNCTION ssp_rng_uniform_uint64

! Uniform random distribution in [lower upper)
 FUNCTION ssp_rng_uniform_double(rng, lower, upper) BIND(C, NAME='ssp_rng_uniform_double')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE), INTENT(IN), VALUE :: lower
  REAL(C_DOUBLE), INTENT(IN), VALUE :: upper
  REAL(C_DOUBLE) :: ssp_rng_uniform_double
 END FUNCTION ssp_rng_uniform_double

 FUNCTION ssp_rng_uniform_float(rng, lower, upper) BIND(C, NAME='ssp_rng_uniform_float')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN), VALUE :: lower
  REAL(C_FLOAT), INTENT(IN), VALUE :: upper
  REAL(C_FLOAT) :: ssp_rng_uniform_float
 END FUNCTION ssp_rng_uniform_float

! Uniform random floating point distribution in [0, 1)
 FUNCTION ssp_rng_canonical(rng) BIND(C, NAME='ssp_rng_canonical')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE) :: ssp_rng_canonical
 END FUNCTION ssp_rng_canonical

 FUNCTION ssp_rng_canonical_float(rng) BIND(C, NAME='ssp_rng_canonical_float')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT) :: ssp_rng_canonical_float
 END FUNCTION ssp_rng_canonical_float

! The returned value is uint64_t C-side
! But ISO_C_BINDING limitations force to declare it as int64_t
! To avoid overflows, this value should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION ssp_rng_min(rng) BIND(C, NAME='ssp_rng_min')
  USE ISO_C_BINDING, ONLY: C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T) :: ssp_rng_min ! See (1)
 END FUNCTION ssp_rng_min

! The returned value is uint64_t C-side
! But ISO_C_BINDING limitations force to declare it as int64_t
! To avoid overflows, this value should be manipulated through TRANSFER
! See (1) and Fortran examples
 FUNCTION ssp_rng_max(rng) BIND(C, NAME='ssp_rng_max')
  USE ISO_C_BINDING, ONLY: C_INT64_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT64_T) :: ssp_rng_max ! See (1)
 END FUNCTION ssp_rng_max

! write internal state in a file
 FUNCTION ssp_rng_write(rng, stream) BIND(C, NAME='ssp_rng_write')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  TYPE(C_PTR), INTENT(IN), VALUE :: stream ! C FILE* expected
  INTEGER(C_INT) :: ssp_rng_write
 END FUNCTION ssp_rng_write

! read internal state from a file
 FUNCTION ssp_rng_read(rng, stream) BIND(C, NAME='ssp_rng_read')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  TYPE(C_PTR), INTENT(IN), VALUE :: stream ! C FILE* expected
  INTEGER(C_INT) :: ssp_rng_read
 END FUNCTION ssp_rng_read

! returns the entropy of a generator as defined by C++11
 FUNCTION ssp_rng_entropy(rng) BIND(C, NAME='ssp_rng_entropy')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE) :: ssp_rng_entropy
 END FUNCTION ssp_rng_entropy

! ******************************************************************************
! Proxy of Random Number Generators - It manages a list of `nbuckets' RNGs
! whose each have independant `infinite' random sequences
! ******************************************************************************
 FUNCTION ssp_rng_proxy_create(allocator, type, nbuckets, proxy) BIND(C, NAME='ssp_rng_proxy_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  IMPORT SSP_RNG_TYPE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nbuckets
  TYPE(C_PTR), INTENT(OUT) :: proxy
  INTEGER(C_INT) :: ssp_rng_proxy_create
 END FUNCTION ssp_rng_proxy_create

 FUNCTION ssp_rng_proxy_create_from_rng(allocator, rng, nbuckets, proxy) BIND(C, NAME='ssp_rng_proxy_create_from_rng')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nbuckets
  TYPE(C_PTR), INTENT(OUT) :: proxy
  INTEGER(C_INT) :: ssp_rng_proxy_create_from_rng
 END FUNCTION ssp_rng_proxy_create_from_rng

 FUNCTION ssp_rng_proxy_ref_get(rng) BIND(C, NAME='ssp_rng_proxy_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT) :: ssp_rng_proxy_ref_get
 END FUNCTION ssp_rng_proxy_ref_get

 FUNCTION ssp_rng_proxy_ref_put(rng) BIND(C, NAME='ssp_rng_proxy_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  INTEGER(C_INT) :: ssp_rng_proxy_ref_put
 END FUNCTION ssp_rng_proxy_ref_put

! Create the RNG of `ibucket'. Return a RES_BAD_ARG error if RNG was already
! created for `ibucket'. Call ssp_rng_ref_put to release the returned RNG
 FUNCTION ssp_rng_proxy_create_rng(proxy, ibuckets, rng) BIND(C, NAME='ssp_rng_proxy_create_rng')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: proxy
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: ibuckets
  TYPE(C_PTR), INTENT(OUT) :: rng
  INTEGER(C_INT) :: ssp_rng_proxy_create_rng
 END FUNCTION ssp_rng_proxy_create_rng

 FUNCTION ssp_rng_proxy_get_type(proxy, type) BIND(C, NAME='ssp_rng_proxy_get_type')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPORT SSP_RNG_TYPE
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: proxy
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: type
  INTEGER(C_INT) :: ssp_rng_proxy_get_type
 END FUNCTION ssp_rng_proxy_get_type

! ******************************************************************************
! General discrete distribution
! ******************************************************************************
! Create a discrete random variate data structure from a list of weights.
! `weights' contain the weights of `nweights' discrete events. Its elements
! must be positive but they needn't add up to one.
 FUNCTION ssp_ran_discrete_create(allocator, ran) BIND(C, NAME='ssp_ran_discrete_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator
  TYPE(C_PTR), INTENT(OUT) :: ran
  INTEGER(C_INT) :: ssp_ran_discrete_create
 END FUNCTION ssp_ran_discrete_create

 FUNCTION ssp_ran_discrete_setup(discrete, weights, nweights) BIND(C, NAME='ssp_ran_discrete_setup')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: discrete
  TYPE(C_PTR), INTENT(IN), VALUE :: weights
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: nweights
  INTEGER(C_INT) :: ssp_ran_discrete_setup
 END FUNCTION ssp_ran_discrete_setup

 FUNCTION ssp_ran_discrete_ref_get(ran) BIND(C, NAME='ssp_ran_discrete_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: ran
  INTEGER(C_INT) :: ssp_ran_discrete_ref_get
 END FUNCTION ssp_ran_discrete_ref_get

 FUNCTION ssp_ran_discrete_ref_put(ran) BIND(C, NAME='ssp_ran_discrete_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: ran
  INTEGER(C_INT) :: ssp_ran_discrete_ref_put
 END FUNCTION ssp_ran_discrete_ref_put

! Return the index of the sampled discret event.
 FUNCTION ssp_ran_discrete(rng, ran) BIND(C, NAME='ssp_ran_discrete')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  TYPE(C_PTR), INTENT(IN), VALUE :: ran
  INTEGER(C_SIZE_T) :: ssp_ran_discrete
 END FUNCTION ssp_ran_discrete

! Return the PDF of the discret event `i'.
 FUNCTION ssp_ran_discrete_pdf(i, ran) BIND(C, NAME='ssp_ran_discrete_pdf')
  USE ISO_C_BINDING, ONLY: C_SIZE_T, C_DOUBLE, C_PTR
  IMPLICIT NONE
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: i
  TYPE(C_PTR), INTENT(IN), VALUE :: ran
  REAL(C_DOUBLE) :: ssp_ran_discrete_pdf
 END FUNCTION ssp_ran_discrete_pdf

!*******************************************************************************
! Miscellaneous distributions
! ******************************************************************************
! Random variate from the exponential distribution with mean `mu':
! P(x) dx =  mu  * exp(-x * mu) dx
 FUNCTION ssp_ran_exp(rng, mu) BIND(C, NAME='ssp_ran_exp')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE), INTENT(IN), VALUE :: mu
  REAL(C_DOUBLE) :: ssp_ran_exp
 END FUNCTION ssp_ran_exp

 FUNCTION ssp_ran_exp_pdf(x, mu) BIND(C, NAME='ssp_ran_exp_pdf')
  USE ISO_C_BINDING, ONLY: C_DOUBLE
  IMPLICIT NONE
  REAL(C_DOUBLE), INTENT(IN), VALUE :: x
  REAL(C_DOUBLE), INTENT(IN), VALUE :: mu
  REAL(C_DOUBLE) :: ssp_ran_exp_pdf
 END FUNCTION ssp_ran_exp_pdf

! Random variate from the gaussian (or normal) distribution with mean `mu':
! P(x) dx = 1 / (sigma*sqrt(2*PI)) * exp(1/2*((x-mu)/sigma)^2) dx
 FUNCTION ssp_ran_gaussian(rng, mu, sigma) BIND(C, NAME='ssp_ran_gaussian')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE), INTENT(IN), VALUE :: mu
  REAL(C_DOUBLE), INTENT(IN), VALUE :: sigma
  REAL(C_DOUBLE) :: ssp_ran_gaussian
 END FUNCTION ssp_ran_gaussian

 FUNCTION ssp_ran_gaussian_pdf(mu, sigma) BIND(C, NAME='ssp_ran_gaussian_pdf')
  USE ISO_C_BINDING, ONLY: C_DOUBLE
  IMPLICIT NONE
  REAL(C_DOUBLE), INTENT(IN), VALUE :: mu
  REAL(C_DOUBLE), INTENT(IN), VALUE :: sigma
  REAL(C_DOUBLE) :: ssp_ran_gaussian_pdf
 END FUNCTION ssp_ran_gaussian_pdf

! Random variate from the lognormal distribution:
! P(x) dx = 1/(sigma*x*sqrt(2*PI)) * exp(-(ln(x)-zeta)^2 / (2*sigma^2)) dx
 FUNCTION ssp_ran_lognormal(rng, zeta, sigma) BIND(C, NAME='ssp_ran_lognormal')
  USE ISO_C_BINDING, ONLY: C_DOUBLE, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_DOUBLE), INTENT(IN), VALUE :: zeta
  REAL(C_DOUBLE), INTENT(IN), VALUE :: sigma
  REAL(C_DOUBLE) :: ssp_ran_lognormal
 END FUNCTION ssp_ran_lognormal

 FUNCTION ssp_ran_lognormal_pdf(x, zeta, sigma) BIND(C, NAME='ssp_ran_lognormal_pdf')
  USE ISO_C_BINDING, ONLY: C_DOUBLE
  IMPLICIT NONE
  REAL(C_DOUBLE), INTENT(IN), VALUE :: x
  REAL(C_DOUBLE), INTENT(IN), VALUE :: zeta
  REAL(C_DOUBLE), INTENT(IN), VALUE :: sigma
  REAL(C_DOUBLE) :: ssp_ran_lognormal_pdf
 END FUNCTION ssp_ran_lognormal_pdf

!*******************************************************************************
! Sphere distribution
! ******************************************************************************
! Uniform sampling of an unit sphere. The PDF of the generated sample is
! stored in sample[3]
 FUNCTION ssp_ran_sphere_uniform(rng, sample) BIND(C, NAME='SSP_RAN_SPHERE_UNIFORM')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_sphere_uniform
 END FUNCTION ssp_ran_sphere_uniform

! Return the probability distribution for a sphere uniform random variate
 FUNCTION ssp_ran_sphere_uniform_pdf() BIND(C, NAME='SSP_RAN_SPHERE_UNIFORM_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT) :: ssp_ran_sphere_uniform_pdf
 END FUNCTION ssp_ran_sphere_uniform_pdf

!*******************************************************************************
! Triangle distribution
! ******************************************************************************
 FUNCTION ssp_ran_triangle_uniform(rng, v0, v1, v2, sample) BIND(C, NAME='SSP_RAN_TRIANGLE_UNIFORM')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: v0(3)
  REAL(C_FLOAT), INTENT(IN) :: v1(3)
  REAL(C_FLOAT), INTENT(IN) :: v2(3)
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_triangle_uniform
 END FUNCTION ssp_ran_triangle_uniform

 FUNCTION ssp_ran_triangle_uniform_pdf(v0, v1, v2) BIND(C, NAME='SSP_RAN_TRIANGLE_UNIFORM_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: v0(3)
  REAL(C_FLOAT), INTENT(IN) :: v1(3)
  REAL(C_FLOAT), INTENT(IN) :: v2(3)
  REAL(C_FLOAT) :: ssp_ran_triangle_uniform_pdf
 END FUNCTION ssp_ran_triangle_uniform_pdf

!*******************************************************************************
! Hemisphere distribution
! ******************************************************************************
! Uniform sampling of an unit hemisphere whose up direction is implicitly the
! Z axis. The PDF of the generated sample is stored in sample[3]
 FUNCTION ssp_ran_hemisphere_uniform_local(rng, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_UNIFORM_LOCAL')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_hemisphere_uniform_local
 END FUNCTION ssp_ran_hemisphere_uniform_local

! Return the probability distribution for an hemispheric uniform random
! variate with an implicit up direction in Z
 FUNCTION ssp_ran_hemisphere_uniform_local_pdf(sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_UNIFORM_LOCAL_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: sample(3)
  REAL(C_FLOAT) :: ssp_ran_hemisphere_uniform_local_pdf
 END FUNCTION ssp_ran_hemisphere_uniform_local_pdf

! Uniform sampling of an unit hemisphere whose up direction is `up'. The PDF
! of the generated sample is stored in sample[3]
 FUNCTION ssp_ran_hemisphere_uniform(rng, up, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_UNIFORM')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: up(3)
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_hemisphere_uniform
 END FUNCTION ssp_ran_hemisphere_uniform

! Return the probability distribution for an hemispheric uniform random
! variate with an explicit `up' direction
 FUNCTION ssp_ran_hemisphere_uniform_pdf(up, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_UNIFORM_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: up(3)
  REAL(C_FLOAT), INTENT(IN) :: sample(3)
  REAL(C_FLOAT) :: ssp_ran_hemisphere_uniform_pdf
 END FUNCTION ssp_ran_hemisphere_uniform_pdf

! Cosine weighted sampling of an unit hemisphere whose up direction is
! implicitly the Z axis. The PDF of the generated sample is stored in
! sample[3]
 FUNCTION ssp_ran_hemisphere_cos_local(rng, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_COS_LOCAL')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_hemisphere_cos_local
 END FUNCTION ssp_ran_hemisphere_cos_local

! Return the probability distribution for an hemispheric cosine weighted
! random variate with an implicit up direction in Z
 FUNCTION ssp_ran_hemisphere_cos_local_pdf(sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_COS_LOCAL_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: sample(3)
  REAL(C_FLOAT) :: ssp_ran_hemisphere_cos_local_pdf
 END FUNCTION ssp_ran_hemisphere_cos_local_pdf

! Cosine weighted sampling of an unit hemisphere whose up direction is `up'.
! The PDF of the generated sample is stored in sample[3]
 FUNCTION ssp_ran_hemisphere_cos(rng, up, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_COS')
  USE ISO_C_BINDING, ONLY: C_FLOAT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: rng
  REAL(C_FLOAT), INTENT(IN) :: up(3)
  REAL(C_FLOAT), INTENT(IN) :: sample(4)
  TYPE(C_PTR) :: ssp_ran_hemisphere_cos
 END FUNCTION ssp_ran_hemisphere_cos

! Return the probability distribution for an hemispheric cosine weighted
! random variate with an explicit `up' direction
 FUNCTION ssp_ran_hemisphere_cos_pdf(up, sample) BIND(C, NAME='SSP_RAN_HEMISPHERE_COS_PDF')
  USE ISO_C_BINDING, ONLY: C_FLOAT
  IMPLICIT NONE
  REAL(C_FLOAT), INTENT(IN) :: up(3)
  REAL(C_FLOAT), INTENT(IN) :: sample(3)
  REAL(C_FLOAT) :: ssp_ran_hemisphere_cos_pdf
 END FUNCTION ssp_ran_hemisphere_cos_pdf

END INTERFACE

CONTAINS
 FUNCTION ssp_rng_type_neq(rng_type_1, rng_type_2)
  USE ISO_C_BINDING, ONLY: C_BOOL, C_PTR
  LOGICAL(C_BOOL) ssp_rng_type_neq
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng_type_1
  TYPE(SSP_RNG_TYPE), INTENT(IN) :: rng_type_2

  ssp_rng_type_neq = .not. ssp_rng_type_eq(rng_type_1, rng_type_2)
 END FUNCTION ssp_rng_type_neq

END MODULE SSP_BINDING
