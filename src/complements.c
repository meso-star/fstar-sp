/* Copyright (C) |Meso|Star> 2015-2016 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the StarSP library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "complements.h"

_Bool SSP_RNG_TYPE_EQ(const struct ssp_rng_type* t0, const struct ssp_rng_type* t1) {
  return ssp_rng_type_eq(t0, t1);
}

float* SSP_RAN_SPHERE_UNIFORM(struct ssp_rng* rng, float sample[4]) {
  return ssp_ran_sphere_uniform(rng, sample);
}

float SSP_RAN_SPHERE_UNIFORM_PDF(void) {
  return ssp_ran_sphere_uniform_pdf();
}

float* SSP_RAN_TRIANGLE_UNIFORM
  (struct ssp_rng* rng,
   const float v0[3], const float v1[3],  const float v2[3],
   float sample[3])
{
  return ssp_ran_triangle_uniform(rng, v0, v1, v2, sample);
}

float SSP_RAN_TRIANGLE_UNIFORM_PDF(const float v0[3], const float v1[3], const float v2[3]) {
  return ssp_ran_triangle_uniform_pdf(v0, v1, v2);
}

FSP_API float* SSP_RAN_HEMISPHERE_UNIFORM_LOCAL(struct ssp_rng* rng, float sample[4]) {
  return ssp_ran_hemisphere_uniform_local(rng, sample);
}

float SSP_RAN_HEMISPHERE_UNIFORM_LOCAL_PDF(const float sample[3]) {
 return ssp_ran_hemisphere_uniform_local_pdf(sample);
}

float* SSP_RAN_HEMISPHERE_UNIFORM (struct ssp_rng* rng, const float up[3], float sample[4]) {
  return ssp_ran_hemisphere_uniform(rng, up, sample);
}

float SSP_RAN_HEMISPHERE_UNIFORM_PDF(const float up[3], const float sample[3]) {
  return ssp_ran_hemisphere_uniform_pdf(up, sample);
}

float* SSP_RAN_HEMISPHERE_COS_LOCAL(struct ssp_rng* rng, float sample[4]) {
  return ssp_ran_hemisphere_cos_local(rng, sample);
}

float SSP_RAN_HEMISPHERE_COS_LOCAL_PDF(const float sample[3]) {
  return ssp_ran_hemisphere_cos_local_pdf(sample);
}

float* SSP_RAN_HEMISPHERE_COS(struct ssp_rng* rng, const float up[3], float sample[4]) {
  return ssp_ran_hemisphere_cos(rng, up, sample);
}

float SSP_RAN_HEMISPHERE_COS_PDF(const float up[3], const float sample[3]) {
  return SSP_RAN_HEMISPHERE_COS_PDF(up, sample);
}

float SSP_RAN_SPHERE_HG_LOCAL_PDF(const double g, const float sample[3]) {
  return ssp_ran_sphere_hg_local_pdf(g, sample);
}

float* SSP_RAN_SPHERE_HG_LOCAL(struct ssp_rng* rng, const double g, float sample[4]) {
  return ssp_ran_sphere_hg_local(rng, g, sample);
}

float SSP_RAN_SPHERE_HG_PDF(const float up[3], const double g, const float sample[3]) {
  return ssp_ran_sphere_hg_pdf(up, g, sample);
}

float* SSP_RAN_SPHERE_HG(struct ssp_rng* rng, const float up[3], const double g, float sample[4]) {
  return ssp_ran_sphere_hg(rng, up, g, sample);
}

